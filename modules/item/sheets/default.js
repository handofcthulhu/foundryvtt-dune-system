// -*- js -*-
export default class ItemSheetDune extends ItemSheet {

    /** @override */
    get template() {
	switch (this.item.type) {
	case "asset":
	    return "systems/dune/templates/items/asset-sheet.html";
	case "trait":
	    return "systems/dune/templates/items/trait-sheet.html";
	default:
	    return "templates/sheets/item-sheet.html";
	}
    }

}
